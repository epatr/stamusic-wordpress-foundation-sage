<article @php(post_class())>
  <header>
    <h2 class="entry-title"><a href="{{ get_permalink() }}">{{ get_the_title() }}</a></h2>
    @include('partials/entry-meta')
  </header>
  <div class="entry-summary">
    
    <img src="http://placehold.it/1200x650">
    
    @php(the_excerpt())
    
    <a class="button" href="{{ get_permalink() }}">Read On</a>
  </div>
</article>
